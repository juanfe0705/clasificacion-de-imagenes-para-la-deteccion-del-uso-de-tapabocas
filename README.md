# Clasificación de imágenes para la detección del uso de tapabocas

Se propone el uso de diversas técnicas computacionales que permitan detectar y validar el uso del tapabocas. En este trabajo se propone la implementación y evaluación de distintas arquitecturas de aprendizaje profundo DNN para llevar acabo una correcta distinción entre imágenes de personas según el uso del tapabocas.

Presentamos tres modelos DNN donde el modelo 1 presenta una mejor representación en la clase desbalanceada “SinTapabocas”, el modelo 2 con la exactitud más alta del 75,74% y el modelo 3 con una menor cantidad de capaz densas y neuronas da una buena representación en el aprendizaje de la clase “ConTapabocas”.

Este proyecto cuenta con un vídeo de presentación en:
- https://youtu.be/Dbjq-8Kjkj8


El dataset utilizado para este trabajo puede se encuentra en: 
- https://www.kaggle.com/wobotintelligence/face-mask-detection-dataset